import ctypes

"""
//-----------------------------
// CROSS SECTION DATA STRUCTURE
//-----------------------------
typedef struct
{
   int           type;            // type code of cross section shape
   int           culvertCode;     // type of culvert (if any)
   int           transect;        // index of transect/shape (if applicable)
   double        yFull;           // depth when full (ft)
   double        wMax;            // width at widest point (ft)
   double        ywMax;           // depth at widest point (ft)
   double        aFull;           // area when full (ft2)
   double        rFull;           // hyd. radius when full (ft)
   double        sFull;           // section factor when full (ft^4/3)
   double        sMax;            // section factor at max. flow (ft^4/3)

   // These variables have different meanings depending on section shape
   double        yBot;            // depth of bottom section
   double        aBot;            // area of bottom section
   double        sBot;            // slope of bottom section
   double        rBot;            // radius of bottom section
}  CXsec;
"""
IDBuffer=ctypes.create_string_buffer(20)


class CXsect(ctypes.Structure):
    _fields_=[
        ('type',ctypes.c_int),
        ('culvertCode',ctypes.c_int),
        ('transect',ctypes.c_int),
        ('yFull',ctypes.c_double),
        ('wMax',ctypes.c_double),
        ('ywMax',ctypes.c_double),
        ('rFull',ctypes.c_double),
        ('sFull',ctypes.c_double),
        ('sMax',ctypes.c_double),

        # These variables have different meanings depending on section shape
        ('yBot',ctypes.c_double),
        ('aBot',ctypes.c_double),
        ('sBot',ctypes.c_double),
        ('rBot',ctypes.c_double)

    ]


class CConduit(ctypes.Structure):
    _fields_=[
        ('length',ctypes.c_double),  # // conduit length (ft)
        ('roughness',ctypes.c_double),  # // Manning's n
        ('barrels',ctypes.c_char),  # // number of barrels
        # -----------------------------
        ('modLength',ctypes.c_double),  # // modified conduit length (ft)
        ('roughFactor',ctypes.c_double),  # // roughness factor for DW routing
        ('slope',ctypes.c_double),  # // slope
        ('beta',ctypes.c_double),  # // discharge factor
        ('qMax',ctypes.c_double),  # // max. flow (cfs)

        ('a2',ctypes.c_double),  # // upstream & downstream areas (ft2)

        ('q2',ctypes.c_double),  # // upstream & downstream flows per barrel (cfs)

        ('q2Old',ctypes.c_double),  # // previous values of q1 & q2 (cfs)          //(5.1.010)
        ('evapLossRate',ctypes.c_double),  # // evaporation rate (cfs)
        ('seepLossRate',ctypes.c_double),  # // seepage rate (cfs)
        ('capacityLimited',ctypes.c_char),  # // capacity limited flag
        ('superCritical',ctypes.c_char),  # // super-critical flow flag
        ('hasLosses',ctypes.c_char),  # // local losses flag
        ('fullState',ctypes.c_char)  # // determines if either or both ends full    //(5.1.008)
    ]


class CLink(ctypes.Structure):
    _fields_=[
        ('ID',ctypes.POINTER(ctypes.c_char*61)),  # // link ID
        ('type',ctypes.c_long),  # // link type code
        ('subIndex',ctypes.c_long),  # // index of link's sub-category
        ('rptFlag',ctypes.c_char),  # // reporting flag
        ('node1',ctypes.c_long),  # // start node index
        ('node2',ctypes.c_long),  # // end node index
        ('offset1',ctypes.c_double),  # // ht. above start node invert (ft)
        ('offset2',ctypes.c_double),  # // ht. above end node invert (ft)
        # ('xsect',ctypes.POINTER(CXsect)),  # // cross section data
        ('q0',ctypes.c_double),  # // initial flow (cfs)
        ('qLimit',ctypes.c_double),  # // constraint on max. flow (cfs)
        ('cLossInlet',ctypes.c_double),  # // inlet loss coeff.
        ('cLossOutlet',ctypes.c_double),  # // outlet loss coeff.
        ('cLossAvg',ctypes.c_double),  # // avg. loss coeff.
        ('seepRate',ctypes.c_double),  # // seepage rate (ft/sec)
        ('hasFlapGate',ctypes.c_long),  # // true if flap gate present
        # -----------------------------
        ('oldFlow',ctypes.c_double),  # // previous flow rate (cfs)
        ('newFlow',ctypes.c_double),  # // current flow rate (cfs)
        ('oldDepth',ctypes.c_double),  # // previous flow depth (ft)
        ('newDepth',ctypes.c_double),  # // current flow depth (ft)
        ('oldVolume',ctypes.c_double),  # // previous flow volume (ft3)
        ('newVolume',ctypes.c_double),  # // current flow volume (ft3)
        ('surfArea1',ctypes.c_double),  # // upstream surface area (ft2)
        ('surfArea2',ctypes.c_double),  # // downstream surface area (ft2)
        ('qFull',ctypes.c_double),  # // flow when full (cfs)
        ('setting',ctypes.c_double),  # // current control setting
        ('targetSetting',ctypes.c_double),  # // target control setting
        ('timeLastSet',ctypes.c_double),  # // time when setting was last changed        //(5.1.010)
        ('froude',ctypes.c_double),  # // Froude number
        ('oldQual',ctypes.c_double),  # // previous quality state
        ('newQual',ctypes.c_double),  # // current quality state
        ('totalLoad',ctypes.c_double),  # // total quality mass loading
        ('flowClass',ctypes.c_long),  # // flow classification
        ('dqdh',ctypes.c_double),  # // change in flow w.r.t. head (ft2/sec)
        ('direction',ctypes.c_char),  # // flow direction flag
        ('bypassed',ctypes.c_char),  # // bypass dynwave calc. flag
        ('normalFlow',ctypes.c_char),  # // normal flow limited flag
        ('inletControl',ctypes.c_char)  # // culvert inlet control flag

    ]
    def id(self):
        id = self.ID.contents.value.decode('ascii')
        return id

class CNode(ctypes.Structure):
    _fields_=[('ID',ctypes.POINTER(ctypes.c_char*61)),  # // node ID
              ('type',ctypes.c_int),  # // node type code
              ('subIndex',ctypes.c_int),  # // index of node's sub-category
              ('rptFlag',ctypes.c_char),  # // reporting flag
              ('invertElev',ctypes.c_double),  # // invert elevation (ft)
              ('initDepth',ctypes.c_double),  # // initial storage level (ft)
              ('fullDepth',ctypes.c_double),  # // dist. from invert to surface (ft)
              ('surDepth',ctypes.c_double),  # // added depth under surcharge (ft)
              ('pondedArea',ctypes.c_double),  # // area filled by ponded water (ft2)
              ('extInflow',ctypes.POINTER(ctypes.c_double)),  # // pointer to external inflow data
              ('dwfInflow',ctypes.POINTER(ctypes.c_double)),  # // pointer to dry weather flow inflow data
              ('rdiiInflow',ctypes.POINTER(ctypes.c_double)),  # // pointer to RDII inflow data
              ('treatment',ctypes.POINTER(ctypes.c_double)),  # // array of treatment data
              # -----------------------------
              ('degree',ctypes.c_int),  # // number of outflow links
              ('updated',ctypes.c_char),  # // true if state has been updated
              ('crownElev',ctypes.c_double),  # // top of highest connecting conduit (ft)
              ('inflow',ctypes.c_double),  # // total inflow (cfs)
              ('outflow',ctypes.c_double),  # // total outflow (cfs)
              ('losses',ctypes.c_double),  # // evap + exfiltration loss (ft3)
              ('oldVolume',ctypes.c_double),  # // previous volume (ft3)
              ('newVolume',ctypes.c_double),  # // current volume (ft3)
              ('fullVolume',ctypes.c_double),  # // max. storage available (ft3)
              ('overflow',ctypes.c_double),  # // overflow rate (cfs)
              ('oldDepth',ctypes.c_double),  # // previous water depth (ft)
              ('newDepth',ctypes.c_double),  # // current water depth (ft)
              ('oldLatFlow',ctypes.c_double),  # // previous lateral inflow (cfs)
              ('newLatFlow',ctypes.c_double),  # // current lateral inflow (cfs)
              ('oldQual',ctypes.c_double),  # // previous quality state
              ('newQual',ctypes.c_double),  # // current quality state
              ('oldFlowInflow',ctypes.c_double),  # // previous flow inflow
              ('oldNetInflow',ctypes.c_double)  # // previous net inflow
              ]
    def id(self):
        id = self.ID.contents.value.decode('ascii')
        return id