# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (c) 2014 Bryant E. McDonnell
#
# Licensed under the terms of the BSD2 License
# See LICENSE.txt for details
# -----------------------------------------------------------------------------

# Third party imports
import pytest

# Local imports
from pyswmm.swmm5 import PySWMM, PYSWMMException
from pyswmm.tests.data import MODEL_WEIR_SETTING_PATH
from pyswmm.utils.fixtures import get_model_files
from cffi import FFI

def test_warning():
    swmmobject = PySWMM(*get_model_files(MODEL_WEIR_SETTING_PATH))
    swmmobject.swmm_open()
    swmmobject.getLinkResult('C2', 0)
    swmmobject.swmm_close()

def test_cffi():

     ffi=FFI()
     ffi.cdef("""
        int main_like(int argv, char *argv[]);
     """)
     lib=ffi.dlopen("/Users/ningchen/hydraulic/pyswmm/pyswmm/lib/macos/swmm5.so")