# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (c) 2014 Bryant E. McDonnell
#
# Licensed under the terms of the BSD2 License
# See LICENSE.txt for details
# -----------------------------------------------------------------------------

# Third party imports
import pytest

# Local imports
from pyswmm.swmm5 import PySWMM, PYSWMMException
from pyswmm.tests.data import MODEL_WEIR_SETTING_PATH
from pyswmm.utils.fixtures import get_model_files
import itertools

class ListNode(object):
     def __init__(self, x,left,right):
         self.val = x
         self.left = left
         self.right  = right
def test_error_rpt_out():
    swmmobject = PySWMM(MODEL_WEIR_SETTING_PATH)
    swmmobject.swmm_open()
def test_level_order():
    # leve7 = ListNode(7,None, None)
    # leve15 = ListNode(15,None, None)
    # leve9 = ListNode(9,None, None)
    # leve20 = ListNode(20,leve7, leve15)
    # leve3 = ListNode(3,leve9, leve20)
    # matrix = [  [1,2,3],  [4,5,6],  [7,8,9]]
    # matrix = [  [ 5, 1, 9,11],  [ 2, 4, 8,10],[13, 3, 6, 7],[15,14,12,16]]
    # matrix =[[1,2],[3,4]]
    # = "5F3Z-2e-9-w",K=4
    result = nextClosestTime(10000000)

    aa = not result

def nextClosestTime( time):
    ans = start = 60 * int(time[:2]) + int(time[3:])
    elapsed = 24 * 60
    allowed = {int(x) for x in time if x != ':'}
    for h1, h2, m1, m2 in itertools.product(allowed, repeat = 4):
        hours, mins = 10 * h1 + h2, 10 * m1 + m2
        if hours < 24 and mins < 60:
            cur = hours * 60 + mins
            cand_elapsed = (cur - start) % (24 * 60)
            if 0 < cand_elapsed < elapsed:
                ans = cur
                elapsed = cand_elapsed

    return "{:02d}:{:02d}".format(*divmod(ans, 60))

def nextClosestTime(time):
    """
    :type time: str
    :rtype: str
    """
    ans=start=60*(int(time[:2]))+int(time[3:])
    elapsed=24*60
    allowed=(int(x) for x in time if x != ':')

    for h1,h2,m1,m2 in itertools.product(allowed,repeat=4):
        hour,mins=h1*10+h2,m1*10+m2
        if (hour < 24) and (mins < 60):
            cur=hour*60+mins
            cur_elapsed=(cur-start)%(24*60)
            if 0 < cur_elapsed < elapsed:
                elpased=cur_elapsed
                ans=cur
    return "{:02d}:{:02d}".format(*divmod(ans,60))
def rotate(matrix):
    """
    :type matrix: List[List[int]]
    :rtype: void Do not return anything, modify matrix in-place instead.
    """
    if (not matrix):
        return matrix
    width=len(matrix)
    for i in range(int(width/2)):
        nRange = width -1-i
        for j in range(i,nRange):
            temp=matrix[i][j]
            matrix[i][j] =matrix[-1-j][i]
            matrix[-1-j][i]=matrix[-1-i][width-j-1]
            matrix[-1-i][width-j-1]=matrix[j][width-i-1]
            matrix[j][width-i-1]=temp


def levelOrder(root):
    def _levelOrder(levelList):
        lVal=[]
        childResult=[]
        for l in levelList:
            if (l):
                if (l.left):
                    childResult.append(l.left)
                if (l.right):
                    childResult.append(l.right)

                lVal.append(l.val)
        result.append(lVal)
        return childResult

    """
    :type root: TreeNode
    :rtype: List[List[int]]
    """

    result=[]

    children=_levelOrder([root])
    while (children):
        children=_levelOrder(children)

    return result


#    swmmobject.swmmExec()
#    swmmobject.swmm_close()
def twoSum( nums, target):

    aDict = {}
    for i in range(len(nums)):
    # for i in range(len(nums)):
        remain = target -nums[i]
        a = nums[i]
        if(a in aDict):
            return (aDict.get(a),i)
        elif(remain >0):
            aDict[remain] =i
    return []
def addTwoNumbers( l1, l2):
    """
    :type l1: ListNode
    :type l2: ListNode
    :rtype: ListNode
    """
    head = None
    newNode = None
    mod = 0
    p , q = l1, l2
    while(p!=None or q!=None):
        oldNode = newNode
        newNode = ListNode(mod)
        if not (oldNode ==None):
            oldNode.next = newNode
        if(head ==None):
            head = newNode
        if(p!=None) and (q!=None):
            mod,remain = divmod((p.val+q.val+ newNode.val),10)
            newNode.val = remain
            p = p.next
            q = q.next
        elif(p!=None):
            mod,remain = divmod((p.val + newNode.val),10)
            newNode.val = remain
            p =p.next
        elif(q!=None):
            mod,remain = divmod((q.val + newNode.val),10)
            newNode.val = q.val
            q= q.next
    if(mod > 0):
        oldNode = newNode
        newNode = ListNode(mod)
        oldNode.next = newNode
    return head

def testTwoSum():
    list1 = [1]
    target = 9
    list2 =[9,9]
    list = addTwoNumbers(list1,list2)
    list =[0,4,3,0]
    target = 0
    twoSum(list,target)

def test_warning():
    swmmobject = PySWMM(*get_model_files(MODEL_WEIR_SETTING_PATH))
    swmmobject.swmm_open()
    swmmobject.getLinkResult('C2', 0)
    swmmobject.swmm_close()


def test_pyswmm_exception():
    swmmobject = PySWMM(*get_model_files(MODEL_WEIR_SETTING_PATH))
    with pytest.raises(PYSWMMException):
        swmmobject.swmm_open()
        print(swmmobject.fileLoaded)
        swmmobject.swmm_open()
        swmmobject.swmm_close()
