# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
# Copyright (c) 2014 Bryant E. McDonnell
#
# Licensed under the terms of the BSD2 License
# See LICENSE.txt for details
# -----------------------------------------------------------------------------

# Standard library imports
import os
import sys

# Local imports
from pyswmm import Links,Subcatchments, Nodes, Simulation
import unittest

# from pyswmm.swmm5 import PySWMM
from pyswmm.tests.data import (MODEL_PUMP_SETTINGS_PATH, MODEL_STORAGE_PUMP,
                               MODEL_WEIR_SETTING_PATH,MODEL_TWOD_SIMPLE_PATH,MODEL_TWOD_TOUGH_PATH)


class testSWMM5(unittest.TestCase):

    def test_links_1(self):
        sim = Simulation(MODEL_TWOD_SIMPLE_PATH)
        print("\n\n\nLINKS\n")
        links = Links(sim)
        print(links._nLinks)
        count = 0
        file = open("./data/tough1.csv", "w")
        file.write("linkin,inletnose,outletnode\n")
        file.flush()
        for link in links:
            count = count + 1
            file.write(link.linkid + ", "+link.inlet_node+ ", "+link.outlet_node+"\n")
            if ((count % 1000)== 0 ):
                print(count)
                file.flush()


        file.close()
        print(count)

    def test_subcatchments_2(self):
        with Simulation(MODEL_TWOD_TOUGH_PATH) as sim:
            for subcatchment in Subcatchments(sim):
                print(subcatchment)
                print(subcatchment.subcatchmentid)
                print(subcatchment.curb_length)
                subcatchment.curb_length = 10
                print(subcatchment.curb_length)


if __name__ == "__main__":
    unittest.main()